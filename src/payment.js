function payment(action, amount) {
    if (amount === undefined){ return 'You must specify the amount' }
    if (action.includes('withdraw')) {
        return `Successfuly withdrew $${amount}`;
    } else if (action.includes('transfer')) {
        return `Successfully transfered $${amount}`;
    } 
    return 'You must specify an action: withdraw or transfer';
}

module.exports = payment;
